import React from 'react';
import CardPage from '../card_page/CardPage.js';
import Nav from '../nav/nav.jsx';

import './app.css';
import Search from "../Search/search";
import { Route, Routes } from 'react-router-dom';

const App = () => {
    return (
        <div className='main'>
            <div className="box">
                <Search />
                <h1>Люди</h1>
                <Nav>
                    
                </Nav>
                <section>
                    <Routes>
                        <Route path='/people' element={<CardPage url={{url : 'people', img : 'characters'}}></CardPage>}></Route>
                        <Route path='/planets' element={<CardPage url={{url : 'planets', img : 'planets'}}></CardPage>}></Route>
                        <Route path='/films' element={<CardPage url={{url : 'films', img : 'films'}}></CardPage>}></Route>
                        <Route path='/species' element={<CardPage url={{url : 'species', img : 'species'}}></CardPage>}></Route>
                        <Route path='/vehicles' element={<CardPage url={{url : 'vehicles', img : 'vehicles'}}></CardPage>}></Route>
                        <Route path='/starships' element={<CardPage url={{url : 'starships', img : 'starships'}}></CardPage>}></Route>
                    </Routes>
                </section>
            </div>
        </div>
    )
}

export default App;