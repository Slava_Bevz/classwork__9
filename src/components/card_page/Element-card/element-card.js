import React from "react";
import Button from 'react-bootstrap/Button'
import Card from "react-bootstrap/Card";

import './element-card.css'

const ElementCard = ({ img, name, gender, birth_year, id}) => {
    return(
        <Card className="element-card">
            <Card.Img variant="top" src={img} />
            <Card.Body>
                <Card.Title>Peaple info</Card.Title>
                    <ul>
                        <li>Имя:{name}</li>
                        <li>Пол: {gender}</li>
                        <li>День рождения: {birth_year}</li>
                    </ul>
                <Button variant="primary">Подробней</Button>
            </Card.Body>
        </Card>
    );
};

export default ElementCard;
